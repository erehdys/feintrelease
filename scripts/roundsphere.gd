extends AnimatedSprite

func _ready():
	self.frame = 0

onready var sv = $"/root/Main/ServerPlayer"
onready var cl = $"/root/Main/ClientPlayer"


func _on_EnderTimer_roundchange():
	print("roundchange")
	print(sv.roundsLeft)
	print(cl.roundsLeft)
	match sv.roundsLeft:
		3:
			match cl.roundsLeft:
				3:
					self.frame = 0
				2:
					self.frame = 2
				1:
					self.frame = 4
				0:
					self.frame = 0
		2:
			match cl.roundsLeft:
				3:
					self.frame = 1
				2:
					self.frame = 5
				1:
					self.frame = 7
				0:
					self.frame = 0
		1:
			match cl.roundsLeft:
				3:
					self.frame = 3
				2:
					self.frame = 7
				1:
					self.frame = 8
				0:
					self.frame = 0
		0: self.frame = 0

