extends SGKinematicBody2D
# local variables
var damaged = ""
var health = 150
var roundsLeft = 3
var buffered = ""
var blockingState = true
var crouchState = false
var movingState = false
var netstatus = "client"
var posSwitch = false
var deadState = false
var counterState = false
var tooclose = false
var input_prefix := "player1_"
var behindYou = false
var distancelimit = SGFixed.ONE
# fixed point variables
var velocity = SGFixedVector2.new().copy()
#var axisVector = SGFixedVector2.new().copy()
var direction = SGFixed.NEG_ONE
var speed = 444444
var svrot = SGFixed.ONE
var clrot = SGFixed.ONE
var axisVector = SGFixedVector2.new().copy()
#var xdiff = int(1)
#var ydiff = int(1)
var angle = int(1)
#var aVx = int(1)
#var aVy = int(1)
#var xdir = int(1)
#var ydir = int(1)
# node paths
onready var serverplayer = $"../ServerPlayer"
onready var clientplayer = $"../ClientPlayer"
onready var animsprite = $svanimsprite
onready var victimsprite = $svvictimsprite
onready var animtimer = $AttackTimer
onready var timer = $"/root/Main/RoundTimer"
onready var roundender = $"/root/Main/EndSplash/REControl/RoundEnder"
onready var hurtbox = $svhurtbox
onready var opponenthitbox = $"../ClientPlayer/CLHITBOX"
# signals
signal attack_event(attack)
signal death
signal roundover
signal defense_event(defense)
# bot variables
var neverblock = false
var neverspin = false
func _ready():
	self.fixed_position = SGFixed.vector2(16777216, 33554432).copy()
	set_global_fixed_rotation(angle)
	sync_to_physics_engine()
	
func _save_state() -> Dictionary:
	return {
		svpos = self.get_global_fixed_position().copy(),
		svrot = self.get_global_fixed_rotation(),
		velocity = velocity.copy(),
		direction = direction,
		health = health,
		isblocking = blockingState,
		ismoving = movingState,
		iscrouching = crouchState,
		dead = deadState,
		buffering = buffered,
		rounds = roundsLeft,
		speed = speed,
		switched = posSwitch,
		damaged = damaged,
		exposed = counterState,
		tooclose = tooclose,
	}

func _load_state(state: Dictionary) -> void:
	self.set_global_fixed_position(state['svpos'].copy())
	self.set_global_fixed_rotation(state['svrot'])
	velocity = state['velocity'].copy()
	direction = state['direction']
	health = state['health']
	blockingState = state['isblocking']
	movingState = state['ismoving']
	crouchState = state['iscrouching']
	deadState = state['dead']
	buffered = state['buffering']
	roundsLeft = state['rounds']
	speed = state['speed']
	posSwitch = state['switched']
	damaged = state['damaged']
	counterState = state['exposed']
	tooclose = state['tooclose']
	sync_to_physics_engine()


func _get_local_input() -> Dictionary:
	var input_vector = Input.get_vector(input_prefix + "back", input_prefix + "forward", input_prefix +  "down", input_prefix +  "up")
	var input := {}
	if input_vector != Vector2.ZERO:
		input["input_vector"] = input_vector
	if Input.is_action_pressed(input_prefix +"crouch"):
		input["crouch"] = true
	if Input.is_action_just_pressed(input_prefix + "up") || Input.is_action_just_pressed(input_prefix + "back") || Input.is_action_just_released(input_prefix + "down") || Input.is_action_just_released(input_prefix + "forward"):
		input["ub"] = true
	if Input.is_action_just_pressed(input_prefix + "down") || Input.is_action_just_pressed(input_prefix + "forward") || Input.is_action_just_released(input_prefix + "up") || Input.is_action_just_released(input_prefix + "back"):
		input["df"] = true
	if Input.is_action_just_pressed(input_prefix + "light_attack"):
		input["la"] = true
	if Input.is_action_just_pressed(input_prefix + "heavy_attack"):
		input["ha"] = true
	return input

func _network_process(input: Dictionary) -> void:
	var input_vector = input.get("input_vector", Vector2.ZERO)
#	print(altvel.x)
#	print(float(angle)/65536*180/PI)
#	print(float(self.get_global_fixed_rotation())/65536*180/PI)
# general checks
	if animtimer.currentatk != "" || animtimer.currentdefense != "":
		speed = int(0)
	if animtimer.currentatk == "" && animtimer.currentdefense == "":
		if movingState == true:
			speed = 444444
		elif blockingState == true:
			speed = 222222
	if deadState == true:
		speed = int(0)
	if animtimer.currentatk != "":
		blockingState = false
	if animtimer.currentdefense != "":
		blockingState = true
# animation checks
	match animtimer.phase:
		"startup":
			counterState = true
			blockingState = false
		"recovery":
			blockingState = false
			counterState = false
		
# behind you check
	var sveyevector = SGFixed.vector2(SGFixed.cos(svrot), SGFixed.sin(svrot)).copy()
	var cleyevector = SGFixed.vector2(SGFixed.cos(clrot), SGFixed.sin(clrot)).copy()
	if sveyevector.angle_to(cleyevector) > -100000 && sveyevector.angle_to(cleyevector) < 100000:
		behindYou = true
	else:
		behindYou = false
# use that to calculate the angle and distance between players
	angle = SGFixed.atan2(
		clientplayer.get_global_fixed_position().y - self.get_global_fixed_position().y,
		clientplayer.get_global_fixed_position().x - self.get_global_fixed_position().x
	)
	var distance = self.get_global_fixed_position().distance_to(clientplayer.get_global_fixed_position())
# use that to calculate the normalized vector between players
#	aVx = SGFixed.div(SGFixed.cos(angle), distance)
#	aVy = SGFixed.div(SGFixed.sin(angle), distance)
# alt physics
#	xdir = SGFixed.div(xdiff, distance)
#	ydir = SGFixed.div(ydiff, distance)
#(rotated) axisVector = SGFixed.vector2(SGFixed.mul(dirspeed, aVy), SGFixed.mul(dirspeed, -aVx))
# sprite checks
	# if the player is going through an attack animation, they are not blocking by definition
	# if the player is moving forward or laterally, they aren't blocking
	if animtimer.currentatk == "" && animtimer.currentdefense == "":
		if input_vector == Vector2(0, 1) || input_vector == Vector2(0, -1):
			blockingState = false
			movingState = true
			animsprite.animation = "moving"
		if input_vector == Vector2(-1, 0):
			if posSwitch == false && animtimer.currentatk == "":
				blockingState = true
				movingState = false
				animsprite.animation = "blocking"
			else:
				blockingState = false
				movingState = true
				animsprite.animation = "moving"
		if input_vector == Vector2(1, 0):
			if posSwitch == false && animtimer.currentatk == "":
				blockingState = false
				movingState = true
				animsprite.animation = "moving"
			else:
				blockingState = true
				movingState = false
				animsprite.animation = "blocking"
# the player is blocking if they aren't doing anything
		if input_vector == Vector2.ZERO && animtimer.currentatk == "":
			if neverblock == false:
				blockingState = true
				movingState = false
				animsprite.animation = "blocking"
			else:
				blockingState = false
				movingState = true
				animsprite.animation = "moving"
# check for crouching, you aren't blocking standing when crouching
	if input.get("crouch", false):
		crouchState = true
		blockingState = false
	elif animtimer.currentdefense != "lowhitstun":
		crouchState = false
		
# inverting directions 
	if clientplayer.get_global_fixed_position().x < self.get_global_fixed_position().x:
		posSwitch = true
	else:
		posSwitch = false

	# normal attacks
	if animtimer.currentatk == "" && animtimer.currentdefense == "":
# jab / low
		if input.get("la", false):
			if input_vector.x == 0 && input_vector != Vector2(0, -1):
				emit_signal("attack_event", "jab")
			if input_vector == Vector2(1, 0):
				match posSwitch:
					true:
						emit_signal("attack_event", "low")
					false:
						emit_signal("attack_event", "jab")
			if input_vector == Vector2(-1, 0):
					match posSwitch:
						true:
							emit_signal("attack_event", "jab")
						false:
							emit_signal("attack_event", "low")
# hook / homing
		if input.get("ha", false):
			if input_vector.x == 0 && input_vector != Vector2(0, -1):
				emit_signal("attack_event", "hook")
			if input_vector == Vector2(1, 0):
					match posSwitch:
						true:
							emit_signal("attack_event", "homing")
						false:
							emit_signal("attack_event", "hook")
			if input_vector == Vector2(-1, 0):
					match posSwitch:
						true:
							emit_signal("attack_event", "hook")
						false:
							emit_signal("attack_event", "homing")
# deathfist / overhead / ucut
		if input.get("ha", false) && input_vector == Vector2(0, -1):
			emit_signal("attack_event", "deathfist")
			print("deathfist")
		if input.get("ha", false) && input_vector == Vector2(0, 1):
			emit_signal("attack_event", "overhead")
			print("overhead")
		if input.get("la", false) && input_vector == Vector2(0, -1):
			emit_signal("attack_event", "ucut")
			print("ucut")
		if input.get("la", false) && input_vector == Vector2(0, 1):
			emit_signal("attack_event", "throw")
			print("throw")
	# buffered attacks
	else:
# jab / low
		if input.get("la", false):
			if input_vector.x == 0 && input_vector != Vector2(0, -1):
				buffered = "jab"
			if input_vector == Vector2(1, 0):
					match posSwitch:
						true:
							buffered = "low"
						false:
							buffered = "jab"
			if input_vector == Vector2(-1, 0):
					match posSwitch:
						true:
							buffered = "jab"
						false:
							buffered = "low"
# hook / homing
		if input.get("ha", false):
			if input_vector.x == 0 && input_vector != Vector2(0, -1):
				buffered = "hook"
			if input_vector == Vector2(1, 0):
					match posSwitch:
						true:
							buffered = "homing"
						false:
							buffered = "hook"
			if input_vector == Vector2(-1, 0):
					match posSwitch:
						true:
							buffered = "hook"
						false:
							buffered = "homing"
		if input.get("ha", false) && input_vector == Vector2(0, -1):
			buffered = "deathfist"
			print("deathfist buffered")
		if input.get("ha", false) && input_vector == Vector2(0, 1):
			buffered = "deathfist"
			print("overhead")
		if input.get("la", false) && input_vector == Vector2(0, -1):
			buffered = "ucut"
			print("ucut")
		if input.get("la", false) && input_vector == Vector2(0, 1):
			buffered = "throw"
# checking the input buffer when an animation ends
	if animtimer.ticks_left == 0:
		match buffered:
			"ucut":
				emit_signal("attack_event", "ucut")
			"jab":
				emit_signal("attack_event", "jab")
			"low":
				emit_signal("attack_event", "low")
			"deathfist":
				emit_signal("attack_event", "deathfist")
			"hook":
				emit_signal("attack_event", "hook")
			"homing":
				emit_signal("attack_event", "homing")
			"overhead":
				emit_signal("attack_event", "overhead")
			"throw":
				emit_signal("attack_event", "throw")
			_:
				victimsprite.stop()
				victimsprite.frame = 0
				animsprite.stop()
				animsprite.frame = 0
# direction changes
	if input.get("ub", false):
		if input_vector != Vector2(1, 0) && input_vector != Vector2(0, -1):
			if posSwitch == false:
				direction = SGFixed.ONE
			else:
				direction = SGFixed.NEG_ONE
	if input.get("df", false):
		if input_vector != Vector2(-1, 0) && input_vector != Vector2(0, 1):
			if posSwitch == false:
				direction = SGFixed.NEG_ONE
			else:
				direction = SGFixed.ONE
# movement
	var dirspeed = SGFixed.mul(direction, speed)
	axisVector = self.get_global_fixed_position().direction_to(clientplayer.get_global_fixed_position()).normalized().copy()	
	if input_vector != Vector2.ZERO && deadState == false:
		if input_vector.y == 0:
			velocity = move_and_slide(axisVector.mul(-dirspeed)).copy()
		else:
			velocity = move_and_slide(axisVector.mul(dirspeed).rotated(-97000)).copy()
# pushback
	if animtimer.currentdefense != "":
		match animtimer.currentdefense:
			"deathfistknockdown":
				velocity = move_and_slide(axisVector.mul(-349999))
			"homingknockdown":
				velocity = move_and_slide(axisVector.mul(-299999))
			"overheadblockstun":
				velocity = move_and_slide(axisVector.mul(-99999))
			"overheadknockdown":
				velocity = move_and_slide(axisVector.mul(-349999))
			"jabhitstun":
				velocity = move_and_slide(axisVector.mul(-99999))
			"jabblockstun":
				velocity = move_and_slide(axisVector.mul(-79999))
			"ucuthitstun":
				velocity = move_and_slide(axisVector.mul(-99999))
			"ucutblockstun":
				velocity = move_and_slide(axisVector.mul(-49999))
			"throwknockdown":
				velocity = move_and_slide(axisVector.mul(-349999))
			"hookknockdown":
				velocity = move_and_slide(axisVector.mul(-349999))
# rotation functions
	if animtimer.currentatk == "" && animtimer.currentdefense == "":
		if neverspin == false:
			set_global_fixed_rotation(angle)
# take the player positions
	svrot = self.get_global_fixed_rotation()
	clrot = clientplayer.get_global_fixed_rotation()
# damage
	if hurtbox.overlaps_area(opponenthitbox) && animtimer.currentdefense == "" && opponenthitbox.monitoring == true:
		damaged = opponenthitbox.currentatk
	match damaged:
		"jab":
			if crouchState == true:
				pass
			else:	
				if blockingState == false || behindYou == true || animtimer.phase == "recovery":
					print("i got jabbed (())")
					print(health)
					if animtimer.currentdefense == "":
						health -= 10
						victimsprite.play("jabhitstun")
						emit_signal("defense_event", "jabhitstun")
					if health <= 0 && deadState != true:
						emit_signal("death")
				else:
					print("what")
					if animtimer.currentdefense == "":
						victimsprite.play("jabblockstun")
						emit_signal("defense_event", "jabblockstun")
		"hook":
			if crouchState == true:
				pass
			else:
				if blockingState == false || behindYou == true || animtimer.phase == "recovery":
					if counterState == true || behindYou == true:
						if animtimer.currentdefense == "":
							health -= 50
							victimsprite.play("hookknockdown")
							emit_signal("defense_event", "hookknockdown")
							print("strong hook")
							if health <= 0 && deadState != true:
								emit_signal("death")
					else:
						if animtimer.currentdefense == "":
							health -= 15
							victimsprite.play("hookhitstun")
							emit_signal("defense_event", "hookhitstun")
						if health <= 0 && deadState != true:
							emit_signal("death")
				else:
					if animtimer.currentdefense == "":
						victimsprite.play("hookblockstun")
						emit_signal("defense_event", "hookblockstun")
		"deathfist":
			print("deathfist hit clientplayer")
			if blockingState == false || behindYou == true || animtimer.phase == "recovery":
				if animtimer.currentdefense == "":
					print("not blocking the deathfist")
					health -= 50
					victimsprite.play("deathfistknockdown")
					emit_signal("defense_event", "deathfistknockdown")
					print(health)
					if health <= 0:
						emit_signal("death")
			else:
				print("explain")
				if animtimer.currentdefense == "":
					victimsprite.play("deathfistblockstun")
					emit_signal("defense_event", "deathfistblockstun")
		"ucut":
			if blockingState == false || behindYou == true || animtimer.phase == "recovery":
				print(health)
				if animtimer.currentdefense == "":
					health -= 15
					victimsprite.play("ucuthitstun")
					emit_signal("defense_event", "ucuthitstun")
				if health <= 0 && deadState != true:
					emit_signal("death")
			else:
				if animtimer.currentdefense == "":
					victimsprite.play("ucutblockstun")
					emit_signal("defense_event", "ucutblockstun")
		"low":
			if crouchState == false || behindYou == true || animtimer.phase == "recovery":
				print(health)
				print("low hit")
				if animtimer.currentdefense == "":
					health -= 15
					print()
					victimsprite.play("lowhitstun")
					crouchState = true
					emit_signal("defense_event", "lowhitstun")
					if health <= 0 && deadState != true:
						emit_signal("death")
			else:
				pass
		"overhead":
			if blockingState == false || behindYou == true || animtimer.phase == "recovery":
				print("not blocking")
				if counterState == true || behindYou == true:
					if animtimer.currentdefense == "":
						health -=50
						victimsprite.play("overheadknockdown")
						emit_signal("defense_event", "overheadknockdown")
						if health <= 0 && deadState != true:
							emit_signal("death")
				else:
					if animtimer.currentdefense == "":
						print("normal overhead hit")
						health -= 20
						print(health)
						victimsprite.play("overheadhitstun")
						emit_signal("defense_event", "overheadhitstun")
						if health <= 0 && deadState != true:
							emit_signal("death")
			else:
				print("i blocked the overhead")
				if animtimer.currentdefense == "":
					victimsprite.play("overheadblockstun")
					emit_signal("defense_event", "overheadblockstun")
		"homing":
			if blockingState == false || behindYou == true || animtimer.phase == "recovery":
				if counterState == true || behindYou == true:
					if animtimer.currentdefense == "":
						health -= 30
						victimsprite.play("homingknockdown")
						emit_signal("defense_event", "homingknockdown")
						print(health)
						if health <= 0 && deadState != true:
							emit_signal("death")
				else: 
					if animtimer.currentdefense == "":
						health -= 20
						victimsprite.play("homingknockdown")
						emit_signal("defense_event", "homingknockdown")
						if health <= 0 && deadState != true:
							emit_signal("death")
			else:
				print("i blocked the homing")
				if animtimer.currentdefense == "":
					victimsprite.play("homingblockstun")
					emit_signal("defense_event", "homingblockstun")
		"throw":
			if crouchState == true:
				pass
			else:
				if animtimer.currentdefense == "":
					health -= 25
					victimsprite.play("throwknockdown")
					emit_signal("defense_event", "throwknockdown")
					if health <= 0 && deadState != true:
						emit_signal("death")
		"wallsplat":
			victimsprite.play("wallsplat")
			emit_signal("defense_event", "wallsplat")
		_:
			pass
	damaged = ""
	sync_to_physics_engine()


func attack_handler(attack):
	victimsprite.stop()
	victimsprite.frame = 0
	animsprite.stop()
	animsprite.frame = 0
	buffered = ""
	print("attack handler called")
	if neverspin == false:
		set_global_fixed_rotation(angle)
	sync_to_physics_engine()
	match attack:
		"jab":
			print("jab attack")
			animsprite.play("jab")
		"ucut":
			print("uppercut attack")
			animsprite.play("ucut")
		"low":
			print("low attack")
			animsprite.play("low")
		"deathfist":
			print("deathfist attack")
			animsprite.play("deathfist")
		"hook":
			print("hook attack")
			animsprite.play("hook")
		"homing":
			print("homing attack")
			animsprite.play("homing")
		"overhead":
			print("overhead attack")
			animsprite.play("overhead")
		"throw":
			print("throw")
			animsprite.play("throw")
func death():
	print("death function called")
	deadState = true
	speed = 0
	victimsprite.play("death")
	emit_signal("roundover")
	
func _on_EnderTimer_roundchange():
	victimsprite.stop()
	animsprite.stop()
	health = 150
	deadState = false
	self.fixed_position = SGFixed.vector2(16777216, 33554432).copy()
	set_global_fixed_rotation(angle)
	sync_to_physics_engine()
	



