extends Camera2D

onready var serverplayer = $"/root/Main/ServerPlayer"
onready var clientplayer = $"/root/Main/ClientPlayer"

func _ready():
	print(serverplayer.get_position())
	print(self.position)

func _process(_delta):
	var middleX = (serverplayer.get_position().x + clientplayer.get_position().x) / 2
	var middleY = (serverplayer.get_position().y + clientplayer.get_position().y) / 2
	self.position = Vector2(middleX, middleY)
	var distance = serverplayer.get_position().distance_to(clientplayer.get_position())
	var zoomMultiplier = distance / 7500
	
	zoom = Vector2(0.6, 0.6) * (1 + zoomMultiplier)
