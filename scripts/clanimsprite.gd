extends AnimatedSprite
onready var cl = get_parent()
onready var victimsprite = $"../clvictimsprite"

func _process(_delta):
	if cl.deadState == true:
		self.visible = false
	if !victimsprite.is_playing():
		self.visible = true
	if self.is_playing():
		self.visible = true
		match self.animation:
			"throw":
				if self.get_frame() == 28:
					print("throwsparks")
					$throwsparks.visible = true
					$throwsparks.play("explosion")
				else:
					$throwsparks.visible = false
	else:
		$throwsparks.visible = false

