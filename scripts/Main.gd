extends SGFixedNode2D
var logging_enabled := true
# net variables
onready var connection_panel = $CanvasLayer/ConnectionPanel
onready var newconnection = $CanvasLayer/NewConnection
onready var matchbuttons = $CanvasLayer/MatchControl/MatchButtons
onready var matchcontrol = $CanvasLayer/MatchControl
onready var main_menu = $CanvasLayer/MainMenu
onready var host_field = $CanvasLayer/ConnectionPanel/GridContainer/HostField
onready var port_field = $CanvasLayer/ConnectionPanel/GridContainer/PortField
onready var message_label = $CanvasLayer/Meta/MessageLabel
onready var sync_lost_label = $CanvasLayer/Meta/SyncLostLabel
onready var serverplayer = $ServerPlayer
onready var clientplayer = $ClientPlayer
onready var arenabg = $ArenaBG
onready var voidbg = $voidBG
onready var menubg = $CanvasLayer/MenuBG
onready var settings_panel = $CanvasLayer/Control/SettingsPanel
onready var roundender = $EndSplash/REControl/RoundEnder
onready var timer = $RoundTimer
onready var timerlabel = $CanvasLayer/Control/TimerLabel
onready var p1hp = $CanvasLayer/P1HPCONTROL/P1HP
onready var p2hp = $CanvasLayer/P2HPCONTROL/P2HP
onready var roundsphere = $CanvasLayer/Control/roundsphere
onready var blocktoggle = $CanvasLayer/CornerControl/BlockToggle
onready var spintoggle = $CanvasLayer/CornerControl/RotationToggle
onready var movelist = $CanvasLayer/MovelistButton/MovelistTexture
onready var charselect = $CanvasLayer/Control/SettingsPanel/GridContainer/CharSelect
onready var charbuttons = $CanvasLayer/Control/SettingsPanel/GridContainer/CharContainer
onready var matchlabel = $CanvasLayer/MatchControl/MatchLabel
onready var stealthlabel = $CanvasLayer/MatchControl/StealthLabel
onready var copybutton = $CanvasLayer/MatchControl/GridContainer/CopyButton
onready var wall = $Wall
onready var offline = false
onready var connectionstatus = ""
onready var realbg = true
onready var fpslabel = $CanvasLayer/Control/FPSLabel
onready var resetbutton = $CanvasLayer/CornerControl/ResetButton
onready var status_container = $CanvasLayer/StatusContainer
onready var settings_button = $CanvasLayer/Control/SettingsPanel/GridContainer/SettingsButton
onready var startbutton = $CanvasLayer/MatchControl/GridContainer/Start


var PeerStatus = preload("res://PeerStatus.tscn");

var game_started := false

var players := {}
var playercounter = 0
var players_ready := {}
var players_score := {}
var players_setup := {}
var players_alive := {}

var nakama_client: NakamaClient
var nakama_session: NakamaSession
var nakama_socket: NakamaSocket
const LOG_FILE_DIRECTORY = 'user://detailed_logs'
const DummyNetworkAdaptor = preload("res://addons/godot-rollback-netcode/DummyNetworkAdaptor.gd")

func _ready() -> void:
	roundender.visible = false
	get_tree().connect("network_peer_connected", self, "_on_network_peer_connected")
	get_tree().connect("network_peer_disconnected", self, "_on_network_peer_disconnected")
	get_tree().connect("server_disconnected", self, "_on_server_disconnected")
	SyncManager.connect("sync_started", self, "_on_SyncManager_sync_started")
	SyncManager.connect("sync_stopped", self, "_on_SyncManager_sync_stopped")
	SyncManager.connect("sync_lost", self, "_on_SyncManager_sync_lost")
	SyncManager.connect("sync_regained", self, "_on_SyncManager_sync_regained")
	SyncManager.connect("sync_error", self, "_on_SyncManager_sync_error")
	# We can configure OnlineMatch before using it:
	OnlineMatch.min_players = 2
	OnlineMatch.max_players = 2
	OnlineMatch.client_version = 'dev'
	OnlineMatch.ice_servers = [{ "urls": ["stun:stun.l.google.com:19302"] }]
	OnlineMatch.use_network_relay = OnlineMatch.NetworkRelay.AUTO
	# Connect to all of OnlineMatch's signals.
	OnlineMatch.connect("error", self, "_on_OnlineMatch_error")
	OnlineMatch.connect("disconnected", self, "_on_OnlineMatch_disconnected")
	OnlineMatch.connect("match_created", self, "_on_OnlineMatch_match_created")
	OnlineMatch.connect("match_joined", self, "_on_OnlineMatch_match_joined")
	OnlineMatch.connect("matchmaker_matched", self, "_on_OnlineMatch_matchmaker_matched")
	OnlineMatch.connect("player_joined", self, "_on_OnlineMatch_player_joined")
	OnlineMatch.connect("player_left", self, "_on_OnlineMatch_player_left")
	OnlineMatch.connect("player_status_changed", self, "_on_OnlineMatch_player_status_changed")
	OnlineMatch.connect("match_ready", self, "_on_OnlineMatch_match_ready")
	OnlineMatch.connect("match_not_ready", self, "_on_OnlineMatch_match_not_ready")

func _on_OnlineMatch_error(message: String) -> void:
	matchlabel.set_text("ERROR: %s" % message)

func _on_OnlineMatch_disconnected() -> void:
	matchlabel.set_text("Disconnected from match.")

func _on_OnlineMatch_match_created(match_id: String) -> void:
	copybutton.visible = true
	startbutton.visible = true
	stealthlabel.set_text(match_id)
	matchlabel.set_text("Private match created: %s" % match_id)
	

func _on_OnlineMatch_match_joined(match_id: String) -> void:
	print("Joined private match: %s" % match_id)
	matchlabel.set_text("Joined private match: %s" % match_id)
	connection_panel.visible = false
	newconnection.visible = false
	matchcontrol.visible = false
	matchbuttons.visible = false
	main_menu.visible = false
	menubg.visible = false
	settings_panel.visible = false
	arenabg.visible = true

func _on_OnlineMatch_matchmaker_matched(players: Dictionary) -> void:
	print("Joined match via matchmaker")
	for player in players.values():
		print ("Player joined: %s" % player.username)

func _on_OnlineMatch_player_joined(player: OnlineMatch.Player) -> void:
	playercounter += 1
	add_player(player.session_id, player.username)
	print("Player joined: %s" % player.username)
	print(playercounter)
	message_label.text = str(playercounter)
	if playercounter == 2:
		message_label.text = "Match is ready to start"
#	if get_tree().is_network_server() && playercounter > 2:
#		message_label.text = "Starting..."
#		print("Since %s is >2, starting" % playercounter)
#		#rpc("setup_match", {mother_seed = johnny.get_seed()})
#		# Give a little time to get ping data.
#		yield(get_tree().create_timer(0.5), "timeout")
		
	
func _on_OnlineMatch_player_left(player: OnlineMatch.Player) -> void:
	print("Player left: %s" % player.username)
	SyncManager.remove_peer(player.peer_id)
	players.erase(player.peer_id)
	players_ready.erase(player.peer_id)

func _on_OnlineMatch_player_status_changed(player: OnlineMatch.Player, status) -> void:
	print("Player status changed: %s -> %s" % [player.username, status])
	if player.peer_id != get_tree().get_network_unique_id() && status == OnlineMatch.PlayerStatus.CONNECTED:
		rpc_id(player.peer_id, "receive_message", "Hi! We're connected now :-)")
	if status == OnlineMatch.PlayerStatus.CONNECTED:
		if player.peer_id != get_tree().get_network_unique_id():
			SyncManager.add_peer(player.peer_id)	
	
	$ServerPlayer.set_network_master(1)
	if get_tree().is_network_server():
		$ClientPlayer.set_network_master(player.peer_id)
	else:
		$ClientPlayer.set_network_master(get_tree().get_network_unique_id())

		#if get_tree().is_network_server():
			#for session_id in players_ready:
				#rpc_id(player.peer_id, "player_ready", session_id)

remote func receive_message(message: String) -> void:
	print("Message from %s: %s" % [get_tree().get_rpc_sender_id(), message])

#func _on_OnlineMatch_match_not_ready() -> void:
#	print("The match isn't ready to start")

#func _on_OnlineMatch_match_ready(players: Dictionary) -> void:
#	matchlabel.set_text("The match is ready to start!")
#	for player in players.values():
#		print ("- %s" % player.username)
#	OnlineMatch.start_playing()
#	if get_tree().is_network_server():
#		rpc("start_game")


#func _on_Ready_pressed():
#	print(OnlineMatch.get_my_session_id())
#	rpc("player_ready", OnlineMatch.get_my_session_id())
	

#func _on_ServerButton_pressed() -> void:
#	#johnny.randomize()
#	connectionstatus = "server"
#	main_menu.visible = false
#	menubg.visible = false
#	connection_panel.visible = false
#	newconnection.visible = false
#	matchcontrol.visible = false
#	matchbuttons.visible = false
#	offline = false
#	settings_panel.visible = true
#	main_menu.visible = false
#	arenabg.visible = true
#	charselect.visible = false
#	charbuttons.visible = false

#func _on_ClientButton_pressed() -> void:
#	var peer = NetworkedMultiplayerENet.new()
#	peer.create_client(host_field.text, int(port_field.text))
#	get_tree().network_peer = peer
#	connectionstatus = "client"
#	main_menu.visible = false
#	menubg.visible = false
#	connection_panel.visible = false
#	newconnection.visible = false
#	matchcontrol.visible = false
#	matchbuttons.visible = false
#	offline = false
#	settings_panel.visible = true
#	main_menu.visible = false
#	arenabg.visible = true
#	charselect.visible = false
#	charbuttons.visible = false

#func _on_network_peer_connected(peer_id: int):
#	print("_on_network_peer_connected")
#	print("peer id is %s" % peer_id)
#	message_label.text = "Connected!"
#	SyncManager.add_peer(peer_id)
#	$ServerPlayer.set_network_master(1)
#	if get_tree().is_network_server():
#		$ClientPlayer.set_network_master(peer_id)
#	else:
#		$ClientPlayer.set_network_master(get_tree().get_network_unique_id())
#	if get_tree().is_network_server():
#		message_label.text = "Starting..."
#		#rpc("setup_match", {mother_seed = johnny.get_seed()})
#
#		# Give a little time to get ping data.
#		yield(get_tree().create_timer(0.5), "timeout")
#		SyncManager.start()
#	connection_panel.visible = false
#	newconnection.visible = false
#	matchcontrol.visible = false
#	matchbuttons.visible = false
#	main_menu.visible = false
#	menubg.visible = false
#	settings_panel.visible = false
#	arenabg.visible = true

func _on_network_peer_disconnected(peer_id: int):
	message_label.text = "Disconnected"
	SyncManager.remove_peer(peer_id)
	
func _on_server_disconnected() -> void:
	_on_network_peer_disconnected(1)

func _on_ResetButton_pressed():
	SyncManager.stop()
	#emit_signal("checkout")
	SyncManager.clear_peers()
	var peer = get_tree().network_peer
	if peer:
		peer.close()
	get_tree().reload_current_scene()

func _on_SyncManager_sync_started() -> void:
	message_label.text = "Started!"
	timer.start()
	if logging_enabled:
		var dir = Directory.new()
		if not dir.dir_exists(LOG_FILE_DIRECTORY):
			dir.make_dir(LOG_FILE_DIRECTORY)
		
		var datetime = OS.get_datetime(true)
		var log_file_name = "%04d%02d%02d-%02d%02d%02d-peer-%d.log" % [
			datetime['year'],
			datetime['month'],
			datetime['day'],
			datetime['hour'],
			datetime['minute'],
			datetime['second'],
			get_tree().get_network_unique_id(),
		]
		
		SyncManager.start_logging(LOG_FILE_DIRECTORY + '/' + log_file_name)
	

func _on_SyncManager_sync_stopped() -> void:
	if logging_enabled:
		SyncManager.stop_logging()
		
func _on_SyncManager_sync_lost() -> void:
	sync_lost_label.visible = true

func _on_SyncManager_sync_regained() -> void:
	sync_lost_label.visible = false

func _on_SyncManager_sync_error(msg: String) -> void:
	message_label.text = "Fatal sync error: " + msg
	sync_lost_label.visible = false
	var peer = get_tree().network_peer
	if peer:
		peer.close()
	SyncManager.clear_peers()

func _on_SettingsButton_pressed():
	#johnny.randomize()
	if connectionstatus == "server":
		var peer = NetworkedMultiplayerENet.new()
		peer.create_server(int(port_field.text), 1)
		get_tree().network_peer = peer
		message_label.text = "Listening..."
	elif connectionstatus == "client":
		var peer = NetworkedMultiplayerENet.new()
		peer.create_client(host_field.text, int(port_field.text))
		get_tree().network_peer = peer
		message_label.text = "Connecting..."
	connection_panel.visible = false
	newconnection.visible = false
	matchcontrol.visible = false
	matchbuttons.visible = false
	main_menu.visible = false
	menubg.visible = false
	settings_panel.visible = false
	arenabg.visible = true
	if offline == true:
		SyncManager.network_adaptor = DummyNetworkAdaptor.new()
		SyncManager.start()
	
func _on_OnlineButton_pressed():
	offline = false
	#connection_panel.visible = true
	newconnection.visible = true
	matchcontrol.visible = true
	matchbuttons.visible = true
	menubg.visible = false
	blocktoggle.visible = false
	spintoggle.visible = false
	arenabg.visible = true
	charselect.visible = false
	charbuttons.visible = false
	settings_button.visible = false

func _on_LocalButton_pressed() -> void:
	print(realbg)
	menubg.visible = false
	#connection_panel.visible = false
	newconnection.visible = false
	matchcontrol.visible = false
	matchbuttons.visible = false
	offline = true
	settings_panel.visible = true
	#main_menu.visible = false
	blocktoggle.visible = true
	spintoggle.visible = true
	arenabg.visible = true
	charselect.visible = true
	charbuttons.visible = true
	settings_button.visible = true


func _on_BlockToggle_toggled(button_pressed):
	if button_pressed == true:
		if $ServerPlayer.input_prefix == "player2_":
			$ServerPlayer.neverblock = true
		if $ClientPlayer.input_prefix == "player2_":
			$ClientPlayer.neverblock = true
	else:
		$ServerPlayer.neverblock = false
		$ClientPlayer.neverblock = false


func _on_RotationToggle_toggled(button_pressed):
	if button_pressed == true:
		if $ServerPlayer.input_prefix == "player2_":
			$ServerPlayer.neverspin = true
		if $ClientPlayer.input_prefix == "player2_":
			$ClientPlayer.neverspin = true
	else:
		$ServerPlayer.neverspin = false
		$ClientPlayer.neverspin = false

func _on_Purple_toggled(button_pressed):
	if button_pressed == true:
		$ClientPlayer.input_prefix = "player2_"
		$ServerPlayer.input_prefix = "player1_"
	else:
		$ClientPlayer.input_prefix = "player1_"
		$ServerPlayer.input_prefix = "player2_"


func _on_Green_toggled(button_pressed):
	if button_pressed == true:
		$ServerPlayer.input_prefix = "player2_"
		$ClientPlayer.input_prefix = "player1_"
	else:
		$ClientPlayer.input_prefix = "player1_"
		$ServerPlayer.input_prefix = "player2_"


func _on_Movelist_toggled(button_pressed):
	if button_pressed == true:
		movelist.visible = true
		p1hp.visible = false
		p2hp.visible = false
		roundsphere.visible = false
		connection_panel.visible = false
		newconnection.visible = false
		timerlabel.visible = false
	else:
		movelist.visible = false
		p1hp.visible = true
		p2hp.visible = true
		roundsphere.visible = true
		timerlabel.visible = true
		

func _on_EnderTimer_timeout():
	wall.visible = true
	message_label.visible = true
	sync_lost_label.visible = true
	fpslabel.visible = true
	p1hp.visible = true
	p2hp.visible = true
	roundsphere.visible = true
	$ClientPlayer.visible = true
	$ServerPlayer.visible = true
	roundender.visible = false
	roundender.stop()
	roundender.frame = 0
	arenabg.visible = true
	voidbg.visible = true
	timerlabel.visible = true
	resetbutton.visible = true
	if offline == true:
		blocktoggle.visible = true
		spintoggle.visible = true

func _on_RoundTimer_timeout():
	wall.visible = false
	message_label.visible = false
	sync_lost_label.visible = false
	fpslabel.visible = false
	p1hp.visible = false
	p2hp.visible = false
	roundsphere.visible = false
	timerlabel.visible = false
	roundender.visible = true
	$ServerPlayer.visible = false
	$ClientPlayer.visible = false
	arenabg.visible = false
	voidbg.visible = false
	blocktoggle.visible = false
	spintoggle.visible = false
	resetbutton.visible = false


func _on_MatchmakingButton_pressed():
	OnlineMatch.start_matchmaking(OnlineMatch.nakama_socket)




func _on_OnlineMatch_error_code(code, message, extra):
	pass # Replace with function body.


func _on_OnlineMatch_webrtc_peer_added(webrtc_peer, player):
	pass # Replace with function body.


func _on_OnlineMatch_webrtc_peer_removed(webrtc_peer, player):
	pass # Replace with function body.

func add_player(session_id: String, username: String) -> void:
	if not status_container.has_node(session_id):
		var status = PeerStatus.instance()
		status_container.add_child(status)
		status.initialize(username)
		status.name = session_id
		
#remotesync func player_ready(session_id: String) -> void:
#	print("getting ready")
#	if get_tree().is_network_server() and not players_ready.has(session_id):
#		players_ready[session_id] = true
#		if players_ready.size() == 2:
#			if OnlineMatch.match_state != OnlineMatch.MatchState.PLAYING:
#				OnlineMatch.start_playing()
#			start_game()
#
#func start_game(immediate: bool = false) -> void:
#	print("start game")
#	if offline == false:
#		players = OnlineMatch.get_player_names_by_peer_id()
#	else:
#		players = {
#			1: "Player1",
#			2: "Player2",
#		}
#	game_start(players, immediate)
#
#func game_start(players: Dictionary, immediate: bool = false) -> void:
#	if offline != true and not immediate:
#		rpc("_do_game_setup", players)
#	else:
#		_do_game_setup(players)
#
## Initializes the game so that it is ready to really start.
#remotesync func _do_game_setup(players: Dictionary, immediate: bool = false) -> void:
#	get_tree().set_pause(true)
#
#	if game_started:
#		game_stop()
#
#	game_started = true
#
#	var player_index := 1
#
#	if offline != false and not immediate:
#		# Tell the host that we've finished setup.
#		rpc_id(1, "_finished_game_setup", get_tree().get_network_unique_id())
#	else:
#		_do_game_start()
#
#remotesync func _do_game_start() -> void:
#	_on_game_started()
#	get_tree().set_pause(false)
#
#func _on_game_started() -> void:
#	# @todo This is kind of a hack - we need a better way to set this always.
#	if players.size() == 0 and offline != false:
#		players = OnlineMatch.get_player_names_by_peer_id()
#
#
#mastersync func _finished_game_setup(player_id: int) -> void:
#	print("finished game setup")
#	players_setup[player_id] = players_alive[player_id]
#	if players_setup.size() == players_alive.size():
#		# Once all clients have finished setup, tell them to start the game.
#		rpc("_do_game_start")
#		connection_panel.visible = false
#		newconnection.visible = false
#		matchcontrol.visible = false
#		matchbuttons.visible = false
#		main_menu.visible = false
#		menubg.visible = false
#		settings_panel.visible = false
#		arenabg.visible = true
#		SyncManager.start()

func game_stop() -> void:
	players_setup.clear()
	players_alive.clear()
	SyncManager.stop()
	emit_signal("checkout")
	SyncManager.clear_peers()
	var peer = get_tree().network_peer
	if peer:
		peer.close()
	get_tree().reload_current_scene()


func _on_Start_pressed():
	yield(get_tree().create_timer(0.5), "timeout")
	SyncManager.start()
	connection_panel.visible = false
	newconnection.visible = false
	matchcontrol.visible = false
	matchbuttons.visible = false
	main_menu.visible = false
	menubg.visible = false
	settings_panel.visible = false
	arenabg.visible = true
	pass # Replace with function body.
