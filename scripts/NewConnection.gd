extends Control
const CREDENTIALS_FILENAME = 'user://credentials.json'
onready var tab_container := $TabContainer
onready var login_email_field := $TabContainer/Login/GridContainer/EmailField
onready var login_password_field := $TabContainer/Login/GridContainer/PasswordField
onready var copybutton = $"../MatchControl/GridContainer/CopyButton"
onready var ui_layer = $"/root/Main/CanvasLayer/Meta/MessageLabel"

var nakama_client: NakamaClient
var nakama_session: NakamaSession
var nakama_socket: NakamaSocket

var nakama_server_key: String = "defaultkey"
var nakama_host: String = 'finland.dgruzd.com'
var nakama_port: int = 7350
var nakama_scheme: String = 'http'

var email: String = ''
var password: String = ''

var _reconnect: bool = false
var _next_screen

func _ready() -> void:
	var file = File.new()
	if file.file_exists(CREDENTIALS_FILENAME):
		print("credentials saved")
		file.open(CREDENTIALS_FILENAME, File.READ)
		var result := JSON.parse(file.get_as_text())
		if result.result is Dictionary:
			email = result.result['email']
			password = result.result['password']
			login_email_field.text = email
			login_password_field.text = password
		file.close()
#	if email != '' and password != '':
	yield(get_tree().create_timer(0.5), "timeout")
	#do_login()

func get_nakama_client() -> NakamaClient:
	if nakama_client == null:
		nakama_client = Nakama.create_client(
			nakama_server_key,
			nakama_host,
			nakama_port,
			nakama_scheme,
			Nakama.DEFAULT_TIMEOUT,
			NakamaLogger.LOG_LEVEL.ERROR)
	
	return nakama_client
	
func _save_credentials() -> void:
	var file = File.new()
	file.open(CREDENTIALS_FILENAME, File.WRITE)
	var credentials = {
		email = email,
		password = password,
	}
	file.store_line(JSON.print(credentials))
	file.close()
	
	# If we have a stored email and password, attempt to login straight away.


func do_login(save_credentials: bool = false) -> void:
	print("logging in")
	if _reconnect:
		ui_layer.set_text("Session expired! Reconnecting...")
	else:
		ui_layer.set_text("Logging in...")
	# Connect to a local Nakama instance using all the default settings.
	nakama_client = get_nakama_client()
	
	nakama_session = yield(nakama_client.authenticate_email_async(email, password, null, false), 'completed')
	
	nakama_socket = Nakama.create_socket_from(nakama_client)	
	yield(nakama_socket.connect_async(nakama_session), "completed")
	if nakama_session.is_exception():
		print("Failed")
		ui_layer.set_text("Unable to connect to Nakama")
		email = ''
		password = ''
	else:
		print("Success")
		if save_credentials:
			_save_credentials()
		nakama_session = nakama_session
		ui_layer.set_text("Connected to Nakama!")
		
		

func _on_LoginButton_pressed() -> void:
	email = login_email_field.text.strip_edges()
	password = login_password_field.text.strip_edges()
	do_login($TabContainer/Login/GridContainer/SaveCheckBox.pressed)

func _on_CreateAccountButton_pressed() -> void:
	email = $"TabContainer/Create Account/GridContainer/EmailField".text.strip_edges()
	password = $"TabContainer/Create Account/GridContainer/PasswordField".text.strip_edges()

	var username = $"TabContainer/Create Account/GridContainer/UsernameField".text.strip_edges()
	var save_credentials = $"TabContainer/Create Account/GridContainer/SaveCheckBox".pressed
	
	if email == '':
		ui_layer.set_text("Must provide email")
		return
	if password == '':
		ui_layer.set_text("Must provide password")
		return
	if username == '':
		ui_layer.set_text("Must provide username")
		return
	
	#visible = false
	ui_layer.set_text("Creating account...")

	var nakama_session = yield(nakama_client.authenticate_email_async(email, password, username, true), "completed")
	
	if nakama_session.is_exception():
		visible = true
		
		var msg = nakama_session.get_exception().message
		# Nakama treats registration as logging in, so this is what we get if the
		# the email is already is use but the password is wrong.
		if msg == 'Invalid credentials.':
			msg = 'E-mail already in use.'
		elif msg == '':
			msg = "Unable to create account"
		ui_layer.set_text(msg)
		
		# We always set Online.nakama_session in case something is yielding
		# on the "session_changed" signal.
		nakama_session = null
	else:
		if save_credentials:
			_save_credentials()
		nakama_session = nakama_session
		ui_layer.set_text("Created new account")
		do_login()

func _on_CreateButton_pressed():
	#copybutton.visible = true
	$"../MatchControl/MatchLabel".visible = true
	$"../MatchControl/GridContainer/MatchField".visible = false
	$"../MatchControl/GridContainer/Connect".visible = false
	#$"../MatchControl/GridContainer/Start".visible = true
	OnlineMatch.create_match(nakama_socket)

func _on_JoinButton_pressed():
	copybutton.visible = false
	$"../MatchControl/MatchLabel".visible = false
	$"../MatchControl/GridContainer/MatchField".visible = true
	$"../MatchControl/GridContainer/Connect".visible = true
	$"../MatchControl/GridContainer/Start".visible = false
	pass

func _on_Connect_pressed():
	copybutton.visible = false
	$"../MatchControl/MatchLabel".visible = false
	$"../MatchControl/GridContainer/MatchField".visible = false
	$"../MatchControl/GridContainer/Connect".visible = false
	var _match_id = $"../MatchControl/GridContainer/MatchField".text
	ui_layer.text = "Joining match: %s" % _match_id
	print("Joining match: %s" % _match_id)
	OnlineMatch.join_match(nakama_socket, _match_id)

func _on_CopyButton_pressed():
	var match_id = $"../MatchControl/StealthLabel".text
	ui_layer.text = "Copied to clipboard: %s" % match_id
	OS.clipboard = match_id

